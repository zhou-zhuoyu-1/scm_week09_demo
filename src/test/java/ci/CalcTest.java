package ci;

import static org.junit.Assert.*;

import org.junit.Ignore;
import org.junit.Test;

public class CalcTest {

	@Test
	public void testAdd() {
		Calc c = new Calc();
		int r = c.add(2, 3);
		assertEquals(5, r);
	}

	@Test
	public void testSubstract() {
		Calc c = new Calc();
		int r = c.substract(5, 3);
		assertEquals(2, r);
	}
  @Ignore
	@Test
	public void testMulitple() {
		Calc c = new Calc();
		int r = c.mulitple(2, 3);
		assertEquals(6, r);
	}

	@Test
	public void testDivid() {
		Calc c = new Calc();
		int r = c.divid(12, 3);
		assertEquals(4, r);
	}
	
	@Test(expected =ArithmeticException.class )
	public void testDivid02() {
		Calc c = new Calc();
		int r = c.divid(12, 0);
		assertEquals(4, r);
	}

}
