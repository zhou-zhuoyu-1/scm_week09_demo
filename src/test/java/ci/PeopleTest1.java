package ci;
import static org.junit.Assert.*;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import ci.GendorException;
import ci.People;


public class PeopleTest1 extends People {

	@Test
	public void test1() {
		People p = new People();
		p.setSex("男");
		String sex2 = p.getSex();
	}
	
	@SuppressWarnings("deprecation")
	@Test
	public void test2() {
		People p = new People();
		p.setSex("女");
		String sex2 = p.getSex();
		Assert.assertEquals("女", sex2);
	
	}
	
	//expected ָ���׳��쳣������
	// timeout����ָ�� ��ʱʱ�䣬��λΪ����
	@Test(expected=GendorException.class,timeout=200)
	public void test3() {
		People p = new People();
		p.setSex("aa");
	}
	
	
	@Rule
	public ExpectedException  ex = ExpectedException.none();
	@Test
	public void test4() {
		People p = new People();
		ex.expect(GendorException.class);
		p.setSex("aa");
	}
	

}
